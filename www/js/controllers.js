angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$state,$localStorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});


	$scope.logOut = function()
	{
		$localStorage.userid = '';
		$localStorage.facebookid = '';
		$localStorage.name = '';
		$localStorage.email = '';
		$localStorage.image = '';
		
		$state.go('app.login');
	}

  
})

.controller('LoginCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$ionicLoading,$q,$ionicHistory) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />';
	
	
	if ($localStorage.userid)
	{
		$ionicHistory.nextViewOptions({
				disableAnimate: true,
				expire: 300
			});
	
		$state.go('app.deals');
	}


	

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	
	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,first_name,last_name&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function() {
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);
							


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						console.log('profile info fail', fail);
					});
				
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
				// but has not authenticated your app
        // Else the person is not logged into Facebook,
				// so we're not sure if they are logged into this app or not.

				//console.log('getLoginStatus', success.status);
 
				$ionicLoading.show({
				  template: 'loading...<ion-spinner icon="spiral"></ion-spinner>'
				});
 

				// Ask the permissions you need. You can learn more about
				// FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };



	
  $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
  {
		//$scope.LogOut();

		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		$scope.fullname = firstname+' '+lastname;
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"pushid" : $rootScope.pushId
			}					
			$http.post($rootScope.Host+'/facebook_connect.php',facebook_data).success(function(data)
			{
			//	if (data.response.userid)
			//	{
					$localStorage.userid = data.response.userid;
					$localStorage.facebookid = id;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;
					$localStorage.image = 'https://graph.facebook.com/'+id+'/picture?type=large';

					$localStorage.temp1 = String(data.response.temp1);
					$localStorage.temp2 = String(data.response.temp2);
					$localStorage.switcher = String(data.response.switcher);

					//$localStorage.startdate = Date.parse(data.response.start);
					//$localStorage.enddate =  new Date(data.response.end).toISOString();


					$localStorage.startdate = data.response.start;
					$localStorage.enddate =  data.response.end;

					
					//alert ($localStorage.startDate)
					//alert ($localStorage.endDate)

					
				    $state.go('app.deals');	
					

			});
	
	  
  } 

})


.controller('DealsCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$timeout) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />'
	
	if (!$localStorage.temp1)
		$scope.globalTemp = "24" ;
	else
		$scope.globalTemp = $localStorage.temp1;
	
	
	if (!$localStorage.temp2)	
		$scope.userTemp = "20";
	else
		$scope.userTemp = $localStorage.temp2;
	
	
	$scope.showGlobalTemp = true;
	$scope.showUserTemp = false;
	$scope.timer = "";
	
	
	$scope.updateSwitcher = function()
	{
		
		if ($localStorage.switcher)
		{
			if($localStorage.switcher == 0)
			$scope.OnOff = false;
			else
			$scope.OnOff = true;
			
		}
		else
		{
			$scope.OnOff = true;
		}		
		
		
	}
	
	$scope.updateSwitcher();
	

	
	
	$scope.setOnOff = function(type)
	{
		if($scope.OnOff)
		{
			$localStorage.switcher = "0";
			$scope.OnOff = false;
		}
		else
		{
			$scope.OnOff = true;
			$localStorage.switcher = "1";
		}
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"user" : $localStorage.userid,
				"switcher" : $localStorage.switcher,
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/update_switch.php',send_params)
			.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});			

		
		//$scope.updateSwitcher();
	}
	
	$scope.changeTemp = function(type)
	{
		$scope.showGlobalTemp = false;
		$scope.showUserTemp = true;	
		$scope.timeOutValue = 10000;
		//$scope.timer = ""
		
		if (type == 1)
		{
			$scope.userTemp++;
		}
		else
		{
			$scope.userTemp--;
		}
			
			console.log("T0 " + $scope.timer)
			
			if($scope.timer == "")
			{
				console.log("T1 " + $scope.timer)
				//$timeout($scope.timer ,$scope.timeOutValue);
			}
			
			$timeout($scope.timer ,$scope.timeOutValue);
			
			$scope.timer = function() {
				$scope.showGlobalTemp = true;
				$scope.showUserTemp = false;
				$scope.timer = "";
				console.log("T2 " + $scope.timer)
			}
			
			
		
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			send_params = 
			{
				"user" : $localStorage.userid,
				"temp2" : $scope.userTemp,
			}
			//console.log(login_params)
			$http.post($rootScope.Host+'/update_temp.php',send_params)
			.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});		
			
			
			
			
		
	}

})


.controller('TimerCtrl', function($scope, $stateParams,$ionicPopup,$http,$rootScope,$state,$localStorage,$ionicSideMenuDelegate,$sce,$timeout,$cordovaDatePicker) 
{
	$scope.navTitle='<img class="title-image" src="img/logo.png" />'

	
	$scope.fields = 
	{
		"start" : "",
		"end" : "",
	}

	$scope.getTimerData = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			//"user" : "1"
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/get_timer.php',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.fields.start = new Date(data[0].start);
			$scope.fields.end = new Date(data[0].end);
		})
		.error(function(data, status, headers, config)
		{

		});
	}
	
	$scope.getTimerData();
	
	
	//alert($localStorage.startdate)
	//alert($localStorage.enddate)

					
	
	
	/*
	 $timeout(function() {
		$scope.fields.start = $localStorage.startdate;
		$scope.fields.end = $localStorage.enddate;
	}, 300);
	*/
	

	$scope.changeDate= function(type)
	{

      var options = {
          
        date: new Date(),
        mode: '',
        //minDate: $scope.olddate, new Date() - 10000,
        //maxDate : $scope.olddate,
        allowOldDates: true,
        allowFutureDates: false,
        doneButtonLabel: 'DONE',
        doneButtonColor: '#0000FF',
        cancelButtonLabel: 'CANCEL',
        cancelButtonColor: '#0000FF'
      };


    $cordovaDatePicker.show(options).then(function(date)
    {

        $scope.fields.originaldate = date;
        
        var myDate = date;
        var month = myDate.getMonth() + 1;
        var day = myDate.getDate();
        
        if(day<10) {
            day='0'+day
        } 
        if(month<10) {
            month='0'+month
        } 
        //.toString().substr(2,2);
        $scope.birthdate = (day +'-'+ month) +'-'+ myDate.getFullYear();
        $scope.birthyear = myDate.getFullYear();

        
        //$scope.ContinueBtn();
        
		});
	}
	
	
	$scope.changeInput = function(type)
	{
		if (type == 0)
		{
			if ($scope.fields.start)
			{
				$localStorage.startdate = $scope.fields.start;
			}
		}
		
		if (type == 1)
		{
			if ($scope.fields.end)
			{
				$localStorage.enddate = $scope.fields.end;	
			}
		}	


	}

$scope.$on("$ionicView.beforeLeave", function(event){


		
	if ($scope.fields.start)
		$localStorage.startdate = $scope.fields.start;
	
	if ($scope.fields.end)
		$localStorage.enddate = $scope.fields.end;

	$scope.updateTimer = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		send_params = 
		{
			"user" : $localStorage.userid,
			"start" : $scope.fields.start,
			"end" : $scope.fields.end,
		}
		//console.log(login_params)
		$http.post($rootScope.Host+'/update_timer.php',send_params)
		.success(function(data, status, headers, config)
		{
			//alert ("ok44");

		})
		.error(function(data, status, headers, config)
		{

		});			
	}
	
	
	$scope.updateTimer();
	


			
	
	
	
	
	
})

	
	
})

